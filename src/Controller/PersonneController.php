<?php

namespace App\Controller;

use App\Repository\PersonneRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class PersonneController
 * @package App\Controller
 *
 * @Route(path="/personne")
 */
class PersonneController extends AbstractController
{
    private $personneRepository;

    public function __construct(PersonneRepository $personneRepository)
    {
        $this->personneRepository = $personneRepository;
    }

    /**
     * @Route("/add", name="add_personne", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        $data = explode('&', $request->getContent());

        $personne = array();

        foreach ($data as $d) {
            $exploded = explode('=', $d);
            $personne[$exploded[0]] = $exploded[1];
        }

        $firstName = $personne['firstName'];
        $name = $personne['name'];
        $birthDate = new \DateTime($personne['birthDate']);

        if (empty($firstName) || empty($name) || empty($birthDate)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $now = new \DateTime();
        $interval = $now->diff($birthDate)->y;

        if ($interval >= 150) {
            throw new NotFoundHttpException('Expecting personne to be birth less than 150 years ago!');
        }

        $this->personneRepository->savePersonne($firstName, $name, $birthDate);

        return new JsonResponse(['status' => 'Personne created!'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/get/{id}", name="get_one_Personne", methods={"GET"})
     */
    public function getOnePersonne($id): JsonResponse
    {
        $personne = $this->personneRepository->findOneBy(['id' => $id]);

        $now = new \DateTime();
        $interval = $now->diff($personne->getBirthDate())->y;

        $data[] = [
            'id' => $personne->getId(),
            'firstName' => $personne->getFirstName(),
            'name' => $personne->getName(),
            'birthDate' => $personne->getBirthDate(),
            'age' => $interval
        ];

        return new JsonResponse(['personne' => $data], Response::HTTP_OK);
    }

    /**
     * @Route("/get-all-personne", name="get_all_Personnes", methods={"GET"})
     */
    public function getAllPersonnes(): JsonResponse
    {
        $personnes = $this->personneRepository->findAll();
        $data = [];

        foreach ($personnes as $personne) {
            $now = new \DateTime();
            $interval = $now->diff($personne->getBirthDate())->y;

            $data[] = [
                'id' => $personne->getId(),
                'firstName' => $personne->getFirstName(),
                'name' => $personne->getName(),
                'birthDate' => $personne->getBirthDate(),
                'age' => $interval
            ];
        }

        usort($data, function($a, $b)
        {
            return strcmp($a['name'], $b['name']);
        });

        return new JsonResponse(['personnes' => $data], Response::HTTP_OK);
    }
}
