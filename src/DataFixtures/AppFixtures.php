<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Personne;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 5; $i++) {
            $personne = new Personne();
            $personne->setFirstName($faker->firstName);
            $personne->setName($faker->lastName);
            $personne->setBirthDate(new \DateTime('1971-08-08'));
            $manager->persist($personne);
        }

        $manager->flush();
    }
}
