<?php

namespace App\Repository;

use App\Entity\Personne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Personne|null find($id, $lockMode = null, $lockVersion = null)
 * @method Personne|null findOneBy(array $criteria, array $orderBy = null)
 * @method Personne[]    findAll()
 * @method Personne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonneRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $manager
    ) {
        parent::__construct($registry, Personne::class);
        $this->manager = $manager;
    }

    public function savePersonne($firstName, $name, $birthDate)
    {
        $newPersonne = new Personne();

        $newPersonne
            ->setFirstName($firstName)
            ->setName($name)
            ->setBirthDate($birthDate);

        $this->manager->persist($newPersonne);
        $this->manager->flush();
    }
}
